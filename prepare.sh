#!/bin/bash

DATACLAYCMD="docker run \
    --network host -u $UID:$GID \
    -v $PWD/cfgfiles/:/home/dataclayusr/dataclay/cfgfiles/:ro \
    -v $PWD/:/workdir/ \
    bscdataclay/client:2.7.dev"

$DATACLAYCMD NewAccount quantum s3cret
$DATACLAYCMD NewDataContract quantum s3cret quantumdataset quantum
$DATACLAYCMD NewModel quantum s3cret dcmodel /workdir/model python
$DATACLAYCMD GetStubs quantum s3cret dcmodel /workdir/stubs

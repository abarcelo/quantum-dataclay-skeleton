from dataclay import DataClayObject, dclayMethod

import numpy as np

# This is just in case you want to use COMPSs in registered @dclayMethod
try:
    from pycompss.api.task import task
    from pycompss.api.parameter import IN, INOUT

except ImportError:
    from dataclay.contrib.dummy_pycompss import task, IN, INOUT


class PersistentBlock(DataClayObject):
    """A Persistent Block class.

    @dclayImport numpy as np
    @ClassField block_data numpy.ndarray
    @ClassField shape tuple
    """
    @dclayMethod(key="anything", return_="anything")
    def __getitem__(self, key):
        return self.block_data[key]

    @dclayMethod(key="anything", value="anything")
    def __setitem__(self, key, value):
        self.block_data[key] = value

    @dclayMethod(index="anything")
    def __delitem__(self, key):
        """Delete an item"""
        del self.block_data[key]

    @dclayMethod(return_="numpy.ndarray")
    def __array__(self):
        """This is for transparent numpy usage."""
        return self.block_data

    @dclayMethod(other="anything", return_="numpy.ndarray")
    def __matmul__(self, other):
        if isinstance(other, PersistentBlock):
            return self.block_data @ other.block_data
        else:
            return self.block_data @ other

    @dclayMethod(other="anything", return_="numpy.ndarray")
    def __add__(self, other):
        if isinstance(other, PersistentBlock):
            return self.block_data + other.block_data
        else:
            return self.block_data + other

    @dclayMethod(shape="tuple")
    def init_zeros(self, shape):
        self.block_data = np.zeros(shape)
        self.shape = shape

    @dclayMethod(shape="tuple")
    def init_random(self, shape):
        self.block_data = np.random.random(shape)
        self.shape = shape

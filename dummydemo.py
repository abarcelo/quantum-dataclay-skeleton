import numpy as np

from dataclay.api import init

# This should become before using registered classes
init()

# This uses registered classes
from rosnet_array.blockarray import BlockArray

if __name__ == "__main__":
    b = BlockArray.zeros((5, 3))

    print("Shape: %s" % (b.shape,))
    print("String representation: %s" % b)
    print("Content:\n%s" % np.array(b))

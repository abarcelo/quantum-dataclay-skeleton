import numpy as np

# Important! Somebody MUST HAVE INITIALIZED DATACLAY BEFORE THIS IMPORT
# otherwise the import will fail. This is typically the main app responsibility.

# DON'T YOU DARE CHANGE THE IMPORT TO `model`. That would make error messages
# a lot more confusing and troubleshooting a real journey of suffering.
# You have been warned.

from dcmodel.block import PersistentBlock

class BlockArray(np.lib.mixins.NDArrayOperatorsMixin):
    def __init__(self, base_block, shape):
        self.base_block = base_block
        self.shape = shape
        self.grid = None
        self.dtype = None
        self.blockshape = None
    
    def __str__(self):
        return "BlockArray(base_block=%r, shape=%r, grid=%r, blockshape=%r, dtype=%r)" % (
            self.base_block,
            self.shape,
            self.grid,
            self.blockshape,
            self.dtype,
        )

    @classmethod
    def zeros(cls, shape, **kwargs):
        new_b = PersistentBlock()
        new_b.make_persistent(**kwargs)
        new_b.init_zeros(shape)

        return cls(new_b, shape)

    def __array__(self):
        return self.base_block.__array__()
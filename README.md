# Skeleton for Quantum+dataClay prototyping

This readme may improve. Now is a tad too telegraphic. Let's get started.

## dataClay CLI tool

Easiest way is to add this alias to your `.bashrc` or equivalent:

```
alias dataclaycmd="docker run \
    --network host -it -u $UID:$GID \
    -v \$PWD/cfgfiles/:/home/dataclayusr/dataclay/cfgfiles/:ro \
    -v \$PWD/:/workdir/ \
    bscdataclay/client:2.7.dev"
```

## dataClay services

We are using `docker-compose`. Just `up` them.

If you need to change the model, remember to `down` in order to clean up.

## Preparing the environment

Once dataClay services are running, there is the need to prepare the environment.

### Creating an account

```
$ dataclaycmd NewAccount quantum s3cret
  **  dataClay command tool **  
 [dataClay] Account created for user quantum
$ dataclaycmd NewDataContract quantum s3cret quantumdataset quantum
  **  dataClay command tool **  
 [dataClay] User quantum is granted access to dataset quantumdataset
```

### Registering data model

The model that will be registered is in the `model` folder.

You can register the class with `NewModel`:

```
$ dataclaycmd NewModel quantum s3cret dcmodel /workdir/model python
  **  dataClay command tool **  
 [dataClay] Namespace dcmodel for python data model has been created.
--- Using global.properties at /home/dataclayusr/dataclay/cfgfiles/global.properties
No global.properties file found. Using default values
Ignoring `__init__.py` at the root of the model folder (do not put classes there!)
Was gonna register: [<dataclay.util.management.classmgr.MetaClass.MetaClass object at 0x7f4414023440>]
Eventually registered: dict_keys(['block.PersistentBlock'])
 ===> The ContractID for the registered classes is:
22e38dbe-555f-405f-a319-8a97686ed5a3
```

Then the stubs can be retrieved with `GetStubs`:

```
$ dataclaycmd GetStubs quantum s3cret dcmodel /workdir/stubs
  **  dataClay command tool **  
--- Using global.properties at /home/dataclayusr/dataclay/cfgfiles/global.properties
No global.properties file found. Using default values
```

The `stubs` folder should now be populated with internal data. You don't (shouldn't) need to touch that.

### Automating previous steps

Once you are sure that everything works (you have done all previous steps and you are sure that all goes smoothly and no errors are shown at any step of the process), then you can use the `prepare.sh` bash script (which performs all the `dataclaycmd` steps sequentially).

## dataClay client library

It's in PyPI, you can install with `pip`:

```
$ pip install dataclay
```

I recommend using a virtual environment.

### About Python versions

For performance shenanigans, dataClay tries to use [the best serialization available](https://docs.python.org/3/library/pickle.html#data-stream-format). This example assumes that your development machine is using Python >= 3.8. If that's not the case you will have issues. Decide how to solve them (either upgrade your local Python or downgrade the `dspython` service Python version, your call).

## Example of application using everything

See `dummydemo.py`.

Typical output at the time of writing this README:

```
$ python dummydemo.py
--- Using global.properties at /home/alex/BSC/quantum-dataclay-skeleton/cfgfiles/global.properties
No global.properties file found. Using default values
Shape: (5, 3)
String representation: BlockArray(base_block=<PersistentBlock (ClassID=40647f81-3efe-4e76-bdc3-9581aae41954) instance with ObjectID=7213368d-9694-4b4c-88a8-312b490a930a>, shape=(5, 3), grid=None, blockshape=None, dtype=None)
Content:
[[0. 0. 0.]
 [0. 0. 0.]
 [0. 0. 0.]
 [0. 0. 0.]
 [0. 0. 0.]]
```
